package week1

data class Point(val x: Int, val y: Int) {
    val neighbours: Set<WeightedPoint>
        get() = setOf(
            WeightedPoint(Point(x - 1, y - 1), 1.5),
            WeightedPoint(Point(x - 1, y + 1), 1.5),
            WeightedPoint(Point(x + 1, y + 1), 1.5),
            WeightedPoint(Point(x + 1, y - 1), 1.5),
            WeightedPoint(Point(x - 1, y), 1.0),
            WeightedPoint(Point(x + 1, y), 1.0),
            WeightedPoint(Point(x, y - 1), 1.0),
            WeightedPoint(Point(x, y + 1), 1.0)
        )

    infix fun distanceTo(other: Point) = Math.hypot((this.x - other.x).toDouble(), (this.y - other.y).toDouble())
}

data class WeightedPoint(val point: Point, val cost: Double)

data class Node(val point: Point, val distance: Double, val cost: Double, val cameFrom: Node?) {
    val path: Set<Point>
        get() = getPath()

    private tailrec fun getPath(node: Node = this, pathPoints: Set<Point> = emptySet()): Set<Point> {
        if (node.cameFrom == null) return pathPoints + node.point
        return getPath(node.cameFrom, pathPoints + node.point)
    }
}

class Map(private val mapString: String) {
    private val mapPoints = getMapPoints(mapString)
    private lateinit var start: Point
    private lateinit var end: Point
    private fun getMapPoints(mapString: String) = mapString
        .split('\n')
        .foldIndexed(emptySet<Point>()) { y, mapPoints, line ->
            line.foldIndexed(mapPoints) { x, acc, c ->
                when (c) {
                    '.' -> acc + Point(x, y)
                    'S' -> {
                        start = Point(x, y)
                        acc + start
                    }
                    'X' -> {
                        end = Point(x, y)
                        acc + end
                    }
                    else -> acc
                }
            }
        }

    fun shortestPath(pathPoints: Set<Point> = findShortestPathPoints()) = mapString
        .split('\n')
        .mapIndexed { y, s ->
            s.mapIndexed { x, c ->
                if (Point(x, y) in pathPoints) '*' else c
            }.joinToString(separator = "")
        }.joinToString(separator = "\n")

    private fun getNeighbourNodes(currNode: Node) = currNode.point.neighbours
        .filter { it.point in mapPoints }
        .map { it -> Node(it.point, it.point distanceTo end, it.cost + currNode.cost, currNode) }

    private tailrec fun findShortestPathPoints(
        unvisited: Set<Node> = setOf(Node(start, start distanceTo end, 0.0, null)),
        visited: Set<Point> = emptySet(),
        currNode: Node = Node(start, start distanceTo end, 0.0, null)
    ): Set<Point> {

        if (currNode.point == end) return currNode.path

        val neighbourNodes = getNeighbourNodes(currNode)
            .filter { it.point !in visited }
            .filter { (thisPoint, _, thisCost) ->
                unvisited.find { (thisPoint == it.point) }?.let { thisCost < it.cost } ?: true
            }

        val unvisitedUpdate = unvisited - currNode + neighbourNodes

        val nextNode = unvisitedUpdate.sortedBy { (_, distance, cost) -> distance + cost }.first()

        return findShortestPathPoints(unvisitedUpdate, visited + currNode.point, nextNode)
    }
}

fun checkPath(mapString: String) = Map(mapString).shortestPath()
fun main(args: Array<String>) {
    val mapString = """
        ..........B.........
        ......X...B.........
        ..........B.........
        ........BBB....S....
        ....................
        """.trimIndent()


    val map = Map(mapString)
    println(map.shortestPath())
}