
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.concurrent.atomic.AtomicLong


// Adding one million numbers in UI thread with Corutines vs in multiple threads
// Thread took : 26 sec
// Coroutines : main thread : 1.6 sec
// Coroutines : parallel threads : 0.5 sec

fun main(args: Array<String>) = runBlocking {
    val c = AtomicLong()
    val uiScope = CoroutineScope(this.coroutineContext)
    val begin = System.currentTimeMillis()
    for (i in 1..1_000_000L) {
        GlobalScope.launch {
            c.addAndGet(i)
        }
//        thread (start = true){
//            c.addAndGet(i)
//        }
    }

//    uiScope.launch { println("""
//        final ${c.get()} thread in ${Thread.currentThread().name}
//        time : ${System.currentTimeMillis() - begin}
//    """.trimIndent()) }
    println("final ${c.get()} thread in ${Thread.currentThread().name}")
    println("time ${System.currentTimeMillis() - begin}")
    println()
}