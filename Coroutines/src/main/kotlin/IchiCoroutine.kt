import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun main(args: Array<String>) {

    GlobalScope.launch {
        delay(1000L)
        println("Coroutine!")
    }
    println("Hello")
    Thread.sleep(1500L)
}