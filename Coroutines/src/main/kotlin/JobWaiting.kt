import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) = runBlocking {
    val uiScope = CoroutineScope(this.coroutineContext)
    val job = uiScope.launch {
        delay(1000L)
        println("Kotlin in ${Thread.currentThread().name}")
    }
    println("Hello in ${Thread.currentThread().name}")
    job.join()
}