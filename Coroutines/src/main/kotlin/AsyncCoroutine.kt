import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking {
    //    val deferred = (0 until 10000).map {
//        GlobalScope.async {
//            delay(1000)
//            println("$it, Thread in ${Thread.currentThread().name}")
//            it
//        }
//    }
//    val sum = deferred.sumBy {
//        println("item : ${it.await()} in ${Thread.currentThread().name}")
//        it.await()
//    }
//    println("Sum : $sum")

    for (i in 0 until 1000_000) { 
        val token = getToken()
        println("Token ${token.await()} $i in Thread : ${Thread.currentThread().name}")
    }
}

suspend fun getToken(): Deferred<String> {
    return coroutineScope {
        val deffered= async {
            delay(1000)
            return@async "pui"
        }
        val onAwait = deffered.onAwait
        deffered

    }

}