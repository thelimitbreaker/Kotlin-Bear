if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'KotlinJSDemo'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'KotlinJSDemo'.");
}
var KotlinJSDemo = function (_, Kotlin) {
  'use strict';
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var trimIndent = Kotlin.kotlin.text.trimIndent_pdl1vz$;
  var Unit = Kotlin.kotlin.Unit;
  function main$lambda(it) {
    println(it);
    return Unit;
  }
  function main(args) {
    println('Hello AssHole');
    var input = 'Haruka';
    var html = trimIndent('\n        <h1 align = "center"> hello Haruka<\/h1>\n        <button id = "btn"> Try me !!! <\/button>\n    ');
    var root = document.getElementById('root');
    root != null ? (root.innerHTML = html) : null;
    var btn = document.getElementById('btn');
    btn != null ? (btn.addEventListener('click', main$lambda), Unit) : null;
  }
  _.main_kand9s$ = main;
  main([]);
  Kotlin.defineModule('KotlinJSDemo', _);
  return _;
}(typeof KotlinJSDemo === 'undefined' ? {} : KotlinJSDemo, kotlin);
