import kotlin.browser.document

fun main(args: Array<String>) {
    println("Hello AssHole")

    val input = "Haruka"
    val html = """
        <h1 align = "center"> hello $input</h1>
        <button id = "btn"> Try me !!! </button>
    """.trimIndent()
    val root = document.getElementById("root")

    root?.innerHTML = html

    val btn = document.getElementById("btn")
    btn?.addEventListener("click", {
        println(it)
    })
}