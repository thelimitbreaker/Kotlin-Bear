public class FunctionStackDemo {

    private static void fun1() {
        System.out.println("Hello from Fun1");
        fun2();
    }

    private static void fun2() {
        System.out.println("Hola from Fun2");
        fun3();
    }

    private static void fun3() {
        System.out.println("Konichiwa from Fun3");
    }

    public static void main(String[] args) {
        fun1();
    }
}
