class QuickSort {

    private fun partition(array: Array<Int>, low: Int, high: Int): Int {
        val pivot = array[high]
        var i = low - 1
        for (j in low until  high) {
            if (array[j] <= pivot) {
                i++
                val temp = array[i]
                array[i] = array[j]
                array[j] = temp
            }
        }
        val temp = array[i + 1]
        array[i + 1] = array[high]
        array[high] = temp
        //partition index
        return i + 1
    }

    tailrec fun sort(array: Array<Int>, low: Int = 0, high: Int = array.size-1) {
        if (low < high) {
            val pi = partition(array, low, high)
            sort(array, low, pi - 1)
            sort(array, pi+1, high)
        }
    }
}

fun main(pui: Array<String>) {
    val array = arrayOf(9, 19, 7, 5, 23, 21)
    QuickSort().sort(array = array)
    println(array.asList())
}