tailrec fun optiFactorial(num: Int, output: Int = 1): Int {
    return if (num == 0) output
    else optiFactorial(num - 1, output * num)
}

fun main(pui: Array<String>) {
    println(optiFactorial(10))
}