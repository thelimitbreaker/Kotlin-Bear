fun factorial(num: Int): Int {
    return if (num == 0) 1 else num * factorial(num - 1)
}

fun main(pui: Array<String>) {
    println(factorial(10))
}