
class QuickSortIterative {

    private fun partition(arr: IntArray, low: Int, high: Int): Int {
        val pivot = arr[high]
        var i = low - 1
        for (j in low until high) {
            if (arr[j] <= pivot) {
                i++

                val temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            }
        }

        val temp = arr[i + 1]
        arr[i + 1] = arr[high]
        arr[high] = temp

        return i + 1
    }

    fun quickSortIterative(arr: IntArray, l: Int, h: Int) {
        var low=l
        var high=h
        // Create an auxiliary stack
        val stack = IntArray(high - low + 1)

        // initialize top of stack
        var top = -1

        // push initial values of l and h to stack
        stack[++top] = low
        stack[++top] = high

        // Keep popping from stack while is not empty
        while (top >= 0) {
            // Pop h and l
            high = stack[top--]
            low = stack[top--]

            // Set pivot element at its correct position
            // in sorted array
            val p = partition(arr, low, high)

            // If there are elements on left side of pivot,
            // then push left side to stack
            if (p - 1 > low) {
                stack[++top] = low
                stack[++top] = p - 1
            }

            // If there are elements on right side of pivot,
            // then push right side to stack
            if (p + 1 < high) {
                stack[++top] = p + 1
                stack[++top] = high
            }
        }
    }
}
